package web

// file path to templates
const (
	indexPath = "res/tmpl/index.tmpl"
	headPath  = "res/tmpl/sub/header.tmpl"
	teaPath   = "res/tmpl/tea.tmpl"
)

var filenames = []string{indexPath, headPath, teaPath}
