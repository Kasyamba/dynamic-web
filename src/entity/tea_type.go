package entity

type TeaType string

const (
	GreenTea TeaType = "green"
	BlackTea TeaType = "black"
	PuerTea  TeaType = "puer"
)

func (t TeaType) IsValid() bool {
	switch t {
	case GreenTea:
	case BlackTea:
	case PuerTea:
	default:
		return false
	}
	return true
}

func GetTeaTypes() []TeaType {
	return []TeaType{PuerTea, GreenTea, BlackTea}
}
