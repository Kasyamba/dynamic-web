package service

import (
	"dynamic-web/src/entity"
	"dynamic-web/src/data"
	"gopkg.in/mgo.v2/bson"
)

//var teaStorage = map[string]*entity.Tea{
//	"f2a4afc3e": entity.DefaultTea(),
//	"34124124":  {Id: "34124124", Title: "Djamapuri", Type: entity.BlackTea},
//}

func GetAll() ([]*entity.Tea, error) {
	return data.NewTeaDao().GetAll()
}

func Get(id bson.ObjectId) (*entity.Tea, error) {
	return data.NewTeaDao().Get(id)
}

func SetTea(tea *entity.Tea) error {
	return data.NewTeaDao().Set(tea)
}

func EditTea(tea *entity.Tea) error {
	return nil
}
