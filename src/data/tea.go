package data

import (
	"dynamic-web/src/entity"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const teaCollectionName = "tea"

type TeaDao struct {
	data
}

func (t TeaDao) GetAll() ([]*entity.Tea, error) {
	result := make([]*entity.Tea, 0)
	return result, t.handleTeaCollection(func(col *mgo.Collection) error {
		return col.Find(nil).All(&result)
	})
}

func (t TeaDao) Get(id bson.ObjectId) (*entity.Tea, error) {
	result := new(entity.Tea)
	return result, t.handleTeaCollection(func(col *mgo.Collection) error {
		return col.FindId(id).One(result)
	})
}

func (t TeaDao) Set(tea *entity.Tea) error {
	return t.handleTeaCollection(func(col *mgo.Collection) error {
		return col.Insert(bson.M{"title": tea.Title, "type": tea.Type})
	})
}

func (t TeaDao) handleTeaCollection(handler func(col *mgo.Collection) error) error {
	col, err := t.GetCollection(teaCollectionName)
	if err != nil {
		return err
	}
	return handler(col)
}

func NewTeaDao() *TeaDao {
	return &TeaDao{newData()}
}
